# Cellpose demo

1. Log in to exajupyter and be sure to request 1 GPU

2. Open a terminal window in Jupyter

3. From terminal, clone this repo to your preferred exacloud directory
```
cd <preferred dir>
git clone https://gitlab.com/eburling/cellpose_demo.git
cd cellpose_demo
```

4. Start by creating conda env executing the following commands in order (type "y" to accept each installation)
```
conda create -n cp python=3.7

conda activate cp

pip install cellpose

pip uninstall mxnet-mkl

pip uninstall mxnet

pip install mxnet-cu102

pip install ipykernel

python -m ipykernel install --user --name cp --display-name "cp"

pip install scikit-image

conda install -c pyviz holoviews bokeh

conda install datashader

```

5. Open the cellpose_demo.ipynb in Jupyter and select the "cp" kernel

6. Try executing the first code cell with all the import statements

